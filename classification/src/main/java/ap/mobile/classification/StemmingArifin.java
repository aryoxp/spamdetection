package ap.mobile.classification;

/**
 * Created by aryo on 6/8/16.
 */
public class StemmingArifin {


    public String[] daftar_kata;
    public String[] awalan1;
    public String[] awalan2;
    public String[] awalan3;
    public String[] akhiran1;
    public String[] akhiran2;
    public String[] akhiran3;

    public StemmingArifin(String[] daftar_kata) {
        this.daftar_kata = daftar_kata;
        this.awalan1 = this.get_awalan1();
        this.awalan2 = this.get_awalan2();
        this.awalan3 = this.get_awalan3();
        this.akhiran1 = this.get_akhiran1();
        this.akhiran2 = this.get_akhiran2();
        this.akhiran3 = this.get_akhiran3();
    }

    public String[] potong_awalan(String kata) {
        kata = kata.toLowerCase();
        String[] awalan1 = this.awalan1;
        String[] awalan2 = this.awalan2;
        String[] awalan3 = this.awalan3;

        //_2hurufAwal = substr(kata, 0, 2);
        String _5hurufAwal = "";
        if(kata.length() > 5) _5hurufAwal = kata.substring(0, 4);
        String _2hurufAwal = kata.substring(0, 1);

        String[] awalan = {"", ""};
        //String awalan[0] = "";
        //String awalan[1] = "";

        for (int i = 0; i < 2; i++) {
            String awalanTmp = "";
            if( _5hurufAwal.equals("menge") || _5hurufAwal.equals("penge") ) {
                awalanTmp = this.potongAwalanMe(kata);
            } else if (_2hurufAwal.equals("me") || _2hurufAwal.equals("pe")) {
                awalanTmp = this.potongAwalanMe(kata);
            } else if (_2hurufAwal.equals("be")) {
                awalanTmp = this.potongAwalanBe(kata);
            } else {
                awalanTmp = this.potongAwalanLainnya(kata);
            }

            if (!awalanTmp.equals("")) {
                int pjgAwalan = awalanTmp.length();
                kata = kata.substring(pjgAwalan, kata.length() - pjgAwalan - 1);

                _2hurufAwal = kata.substring(0, 1);

                if (Preprocessor.in_array(awalanTmp, awalan2)) {

                    awalan[1] = awalanTmp;

                } else {
                    if (awalan[0] == "") {
                        awalan[0] = awalanTmp;
                    } else {
                        awalan[1] = awalanTmp;
                    }

                }

            }
        }
        return awalan;
    }

    public String[] potong_akhiran(String kata) {
        //jadikan huruf kecil semua
        kata = kata.toLowerCase();
        String[] akhiran1 = this.akhiran1;
        String[] akhiran2 = this.akhiran2;
        String[] akhiran3 = this.akhiran3;
        String[] akhir = {"","",""};
        String _3hurufAkhir = kata.substring(kata.length() - 3);
        String _2hurufAkhir = kata.substring(kata.length() - 2);
        String _1hurufAkhir = kata.substring(kata.length() - 1);
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                if (Preprocessor.in_array(_3hurufAkhir, akhiran1)) {
                    akhir[i] = _3hurufAkhir;
                    //potong kata
                    //kata = substr(kata, 0, strlen(kata) - 3);
                    kata = kata.substring(0, kata.length() - 4);
                    //deklarasi ulang akhiran
                    if(kata.length() > 2) {
                        _3hurufAkhir = kata.substring(kata.length() - 3);
                        _2hurufAkhir = kata.substring(kata.length() - 2);
                        _1hurufAkhir = kata.substring(kata.length() - 1);
                    }
                }
            } else if (i == 1) {
                if (Preprocessor.in_array(_3hurufAkhir, akhiran2)) {
                    akhir[i] = _3hurufAkhir;
                    //potong kata
                    //kata = substr(kata, 0, strlen(kata) - 3);
                    kata = kata.substring(0, kata.length() - 4);
                    //deklarasi ulang akhiran
                    if(kata.length() > 2) {
                        _3hurufAkhir = kata.substring(kata.length() - 3);
                        _2hurufAkhir = kata.substring(kata.length() - 2);
                        _1hurufAkhir = kata.substring(kata.length() - 1);
                    }
                } else if (Preprocessor.in_array(_2hurufAkhir, akhiran2)) {
                    akhir[i] = _2hurufAkhir;
                    //potong kata
                    //kata = substr(kata, 0, strlen(kata) - 2);
                    kata = kata.substring(0, kata.length() - 3);
                    //deklarasi ulang akhiran
                    if(kata.length() > 2) {
                        _3hurufAkhir = kata.substring(kata.length() - 3);
                        _2hurufAkhir = kata.substring(kata.length() - 2);
                        _1hurufAkhir = kata.substring(kata.length() - 1);
                    }
                }
            } else {
                if (Preprocessor.in_array(_3hurufAkhir, akhiran3)) {
                    akhir[i] = _3hurufAkhir;
                } else if (Preprocessor.in_array(_2hurufAkhir, akhiran3)) {
                    akhir[i] = _2hurufAkhir;
                } else if (Preprocessor.in_array(_1hurufAkhir, akhiran3)) {
                    akhir[i] = _1hurufAkhir;
                }
            }
        }
        return akhir;
    }

    String potongAwalanMe(String kata) {
        String _2hurufAwal = kata.substring(0, 1);
        String awalan = "";
        String _5hurufAwal = "";
        if(kata.length() > 4)
            _5hurufAwal = kata.substring(0, 4);
        String _4hurufAwal = "";
        if(kata.length() > 3)
            _4hurufAwal = kata.substring(0, 3);
        String _3hurufAwal = "";
        if(kata.length() > 2)
            _4hurufAwal = kata.substring(0, 2);
        String hurufPengganti = "";


        if (_2hurufAwal.equals("me") && Preprocessor.in_array(kata.substring(2, kata.length()-1), this.daftar_kata)) {
            //echo "x";
            awalan = _2hurufAwal;
        } else if (_5hurufAwal.equals("menge") || _5hurufAwal.equals("penge")) {
            awalan = _5hurufAwal;
        } else if (_4hurufAwal.equals("meng") || _4hurufAwal.equals("peng")) {
            //echo "y";
            awalan = _4hurufAwal;
            // hasil yang dipotong tambahkan dengan salah satu huruf berikut ini
            //hurufPengganti=['v','k','g','h','q'];
        } else if (_4hurufAwal.equals("meny") || _4hurufAwal.equals("peny")) {
            //awalan = _4hurufAwal.substr(0,2);
            awalan = _4hurufAwal;
            // ganti ny dengan huruf pengganti
            //hurufPengganti=['s'];
        } else if (_3hurufAwal.equals("mem") || _3hurufAwal.equals("pem")) {
            awalan = _3hurufAwal;
            // tambahkan huruf pengganti jika hasil kata yang dipotong berupa huruf vokal
            //hurufPengganti=['b','f','p','v'];
        } else if (_3hurufAwal.equals("men") || _3hurufAwal.equals("pen")) {
            awalan = _3hurufAwal;
            // tambahkan huruf pengganti jika hasil kata yang dipotong berupa huruf vokal
            //hurufPengganti=['c','d','j','s','t','z'];
        } else if (_3hurufAwal.equals("per")) {
            awalan = _3hurufAwal;
            // hurufPengganti=['c','d','j','s','t','z'];
        } else if (_2hurufAwal.equals("me") || _2hurufAwal.equals("pe")) {
            awalan = _2hurufAwal;
            // hurufPengganti=['l','m','n','r','y','w'];
        }
        return awalan;
        // return awalan+" "+kata.substr(awalan.length)+" "+hurufPengganti;
    }

    String potongAwalanBe(String kata) {
        String awalan = "";
        String hurufPengganti = "";
        String _2hurufAwal = kata.substring(0, 1);
        String _3hurufAwal = kata.substring(0, 2);
        String _huruf_ke_4 = kata.substring(3, 3);


        if (_2hurufAwal.equals("be") && kata.equals("bekerja")) {
            awalan = _2hurufAwal;
        } else if (_3hurufAwal.equals("bel") && kata.equals("belajar")) {
            awalan = _3hurufAwal;
        } else if (_2hurufAwal == "be") {
            if (Preprocessor.in_array(kata.substring(2, kata.length()-1), this.daftar_kata)) {
                awalan = _2hurufAwal;
            } else {
                awalan = _3hurufAwal;
            }
        }
        return awalan;
        // return awalan+" "+kata.substr(awalan.length)+" "+hurufPengganti;
    }

    public String potongAwalanLainnya(String kata) {
        String awalan = "";
        String[] awalanLain = new String[] {"di", "ke", "ku", "se"};
        //hurufPengganti="";
        String _2hurufAwal = kata.substring(0, 1);
        String _3hurufAwal = kata.substring(0, 2);
        if (_3hurufAwal == "ter") {
            awalan = _3hurufAwal;
            // hurufPengganti=['r'];
        } else if (Preprocessor.in_array(_2hurufAwal, awalanLain)) {
            awalan = _2hurufAwal;
        }
        return awalan;
        // return awalan+" "+kata.substr(awalan.length)+" "+hurufPengganti;
    }

    public String[] get_awalan1() {
        return new String[] {"me", "di", "ke", "pe", "se", "be"};
    }

    public String[] get_awalan2() {
        return new String[] {"ber", "ter", "per"};
    }

    public String[] get_awalan3() {
        return new String[] {"menge", "penge"};
    }

    //AKHIRAN
    public String[] get_akhiran1() {
        return new String[] {"lah", "kah", "pun", "tah"};
    }

    public String[] get_akhiran2() {
        return new String[] {"ku", "mu", "nya"};
    }

    public String[] get_akhiran3() {
        return new String[] {"i", "an", "kan"};
    }

}