package ap.mobile.classification;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import ap.mobile.classification.Base.ClassModel;
import ap.mobile.classification.Base.Result;
import ap.mobile.classification.Base.Tweet;

/**
 * Created by aryo on 5/8/16.
 */
public class Classifier {

    private String num;
    private ArrayList<Tweet> tweets;
    private ClassifierProgress iProgress;
    private ClassModel classModel;

    private ArrayList<String> stopwords;
    private ArrayList<String> dictionary;

    public Classifier(ArrayList<Tweet> tweets, ClassifierProgress iProgress, String num) {
        this.tweets = tweets;
        this.iProgress = iProgress;
        this.num = num;
    }

    public Result classify() {
        Log.d("Classifier: Bayes : ", "Start: " + System.nanoTime());
        Result results = new Result();
        try {

            if (this.iProgress != null)
                this.iProgress.onTraining();
            this.classModel = this.trainClassifier();
            Log.d("Classifier: Bayes : ", "Training finished: " + System.nanoTime());

            if (this.iProgress != null)
                this.iProgress.onPreprocessing();
            this.doPreprocessing();
            Log.d("Classifier: Bayes : ", "Preprocessing finished: " + System.nanoTime());

            if (this.iProgress != null)
                this.iProgress.onClassifying();

            for(Tweet t: this.tweets) {
                double posteriorHam = this.classModel.getPosterior(t.words, ClassModel.Category.HAM);
                double posteriorSpam = this.classModel.getPosterior(t.words, ClassModel.Category.SPAM);
                t.category = (posteriorHam > posteriorSpam)? Tweet.TweetCategory.HAM: Tweet.TweetCategory.SPAM;
                if(t.category == Tweet.TweetCategory.SPAM)
                    results.addSpam(t);
                else results.addHam(t);
            }
            Log.d("Classifier: Bayes : ", "Classifying finished: " + System.nanoTime());

            if(this.iProgress!= null) {
                Log.d("Classifier", "Completed!");
                this.iProgress.onComplete();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // TODO: perform Naive Bayes classifying

        return results;
    }

    public ClassModel trainClassifier() {

        //read training data
        String trainingFile = "datatraining" + this.num + ".txt";
        InputStream inTraining = this.getClass().getClassLoader().getResourceAsStream(trainingFile);
        try {
            ArrayList<Tweet> trainingTweets = new ArrayList<>();
            String rawContent = streamToString(inTraining);
            String[] lines = rawContent.split("\n");
            for (int i = 0; i < lines.length; i++) {
                if(lines[i].trim().isEmpty()) continue;
                Tweet tweet = new Tweet();
                String[] data = lines[i].split(",indott,");
                if(data.length == 2) {
                    tweet.text = data[0].trim();
                    tweet.category = data[1].trim().equalsIgnoreCase("Spam") ? Tweet.TweetCategory.SPAM : Tweet.TweetCategory.HAM;
                    trainingTweets.add(tweet);
                }
            }

            //Log.d("Bayes Classifier", "Doc size: " + lines.length);

            trainingTweets = this.doPreprocessing(trainingTweets);

            ClassModel classModel = new ClassModel();
            for (Tweet t:trainingTweets) {
                if(t.category == Tweet.TweetCategory.HAM) classModel.addHam();
                else classModel.addSpam();
                for (String word: t.words) {
                    ClassModel.Category category =
                            (t.category == Tweet.TweetCategory.HAM)? ClassModel.Category.HAM : ClassModel.Category.SPAM;
                    classModel.addWord(word, category);
                }
            }
            //Log.d("Classifier", "Training complete.");

            classModel.updatePrior();

            return classModel;
        } catch (Exception ex) {
            ex.printStackTrace();
            //Log.e("Classifier", );
        }

        return null;

    }

    private void doPreprocessing() {
        this.tweets = doPreprocessing(this.tweets);
    }

    private ArrayList<Tweet> doPreprocessing(ArrayList<Tweet> tweets) {
        ArrayList<Tweet> preprocessedTweet = new ArrayList<>();
        String stopwordsFile = "stopwords";
        String dictionaryFile = "kamuskata";
        InputStream inStopwords = this.getClass().getClassLoader().getResourceAsStream(stopwordsFile);
        InputStream inDictionary = this.getClass().getClassLoader().getResourceAsStream(dictionaryFile);
        try {
            String stopwordsString = streamToString(inStopwords);
            String dictionaryString = streamToString(inDictionary);
            this.stopwords = new ArrayList<>(Arrays.asList(stopwordsString.split("\n")));
            this.dictionary = new ArrayList<>(Arrays.asList(dictionaryString.split("\n")));

            //Log.d("Classifier", "Preprocessing " + tweets.size() + " tweets...");
            for(int i = 0; i<tweets.size(); i++) {
                Tweet t = tweets.get(i);
                //Log.d("Classifier", "Preprocessing: " + i + " : " +  t.text);
                if(this.iProgress != null)
                    this.iProgress.onCleaning();
                String cleanText = Preprocessor.clean(t.text);

                if(this.iProgress != null)
                    this.iProgress.onTokenizing();
                ArrayList<String> words = Preprocessor.tokenize(cleanText);
                //int size = words.size();

                if(this.iProgress != null)
                    this.iProgress.onRemovingStopwords();
                words = Preprocessor.removeStopwords(words, this.stopwords);

                //if(this.iProgress != null)
                //    this.iProgress.onStemming();
                //words = Preprocessor.stem(words, this.dictionary);
                t.words = words;
                preprocessedTweet.add(t);
                //Log.d("Classifier", "Preprocessing tweet [ " + (i+1) + " ] completed. Words count: " + size + " to " + words.size());
                //Log.d("Classifier", "Preprocessing tweet [ " + (i+1) + " ] " + TextUtils.join(" ", words));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            //Log.e("Classifier", ex.getMessage());
        }
        return preprocessedTweet;
    }

    private String streamToString(InputStream is) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        String str = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while ((str = reader.readLine()) != null) {
                if(!str.trim().isEmpty())
                    stringBuffer.append(str + "\n" );
            }
        } finally {
            try { is.close(); } catch (Throwable ignore) {}
        }
        return stringBuffer.toString();
    }

    public interface ClassifierProgress {
        void onCleaning();
        void onTokenizing();
        void onTraining();
        void onStemming();
        void onRemovingStopwords();
        void onPreprocessing();
        void onComplete();
        void onClassifying();
        void onCalculateTFIDF();
    }


}
