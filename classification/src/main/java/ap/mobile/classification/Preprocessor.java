package ap.mobile.classification;

import java.util.ArrayList;
import java.util.Arrays;

public class Preprocessor {

    public static String clean(String content) {
        return content.replaceAll("[^A-Za-z]", " ");
    }

    public static ArrayList<String> tokenize(String content) {
        return new ArrayList<>(Arrays.asList(content.split("( )+")));
    }

    public static ArrayList<String> removeStopwords(ArrayList<String> words, ArrayList<String> stopwords) {
        for (int i = 0; i < words.size(); i++) {
            if (stopwords.contains(words.get(i)) || words.get(i).isEmpty()) {
                //Log.d("Preprocessor", "Removing: " + words.get(i));
                words.remove(i);
            }
        }
        return words;
    }

    public static ArrayList<String> stem(ArrayList<String> words, ArrayList<String> dictionary) {

        String[] daftar_kata = dictionary.toArray(new String[dictionary.size()]);
        String tempKataDasar = "";

        for (int i = 0; i < words.size(); i++) {
            if (!dictionary.contains(words.get(i))) {
                String content = words.get(i).toLowerCase();
                if(content.length() < 4) continue;
                //Log.d("Preprocessor", "Stemming: " + content + " ...");
                if (Preprocessor.in_array(content, daftar_kata)) {
                    //Log.d("Preprocessor", "Content: " + content + " exists in dictionary, skip stemming...");
                    continue;
                }
                StemmingArifin stemming_arifin = new StemmingArifin(daftar_kata);

                String[] awalan = stemming_arifin.potong_awalan(content);
                int panjang_2_awalan = awalan[0].length() + awalan[1].length();

                String[] akhiran = stemming_arifin.potong_akhiran(content);
                int panjang_3_akhiran = akhiran[0].length() + akhiran[1].length() + akhiran[2].length();


                String kataDasar = content.substring(panjang_2_awalan, content.length() - (panjang_3_akhiran + panjang_2_awalan));
                //echo kataDasar;
                String _2hurufAkhirAwalan = "";
                if(awalan[0].length() > 3) {
                    _2hurufAkhirAwalan = awalan[0].substring(2, 3);
                    //Log.d("Preprocessor", "Found awalan: " + awalan[0]);
                }
                String _1hurufAkhirAwalan = "";
                if(awalan[0].length() > 2) {
                    _1hurufAkhirAwalan = awalan[0].substring(2, 2);
                    //Log.d("Preprocessor", "Found awalan: " + awalan[0]);
                }


                if (_2hurufAkhirAwalan.equals("ng")) {
                    // untuk kata seperti kontak, kantuk akan dilebur menjadi mengantuk
                    // tambahkan dengan huruf k
                    tempKataDasar = "k" + kataDasar;
                    if (Preprocessor.in_array(tempKataDasar, daftar_kata)) {
                        //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                        words.remove(i);
                        words.add(i, tempKataDasar);
                        continue;
                    }

                }

                if (_2hurufAkhirAwalan.equals("ny")) {
                    // tambahkan dengan huruf s
                    tempKataDasar = "s" + kataDasar;
                    if (in_array(tempKataDasar, daftar_kata)) {
                        //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                        words.remove(i);
                        words.add(i, tempKataDasar);
                        continue;
                    }
                }

                if (_1hurufAkhirAwalan.equals("m")) {
                    // tambahkan dengan huruf p
                    tempKataDasar = "p" + kataDasar;
                    if (in_array(tempKataDasar, daftar_kata)) {
                        //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                        words.remove(i);
                        words.add(i, tempKataDasar);
                        continue;
                    }
                }

                if (_1hurufAkhirAwalan.equals("n")) {
                    // tambahkan dengan huruf t
                    tempKataDasar = "t" + kataDasar;
                    if (in_array(tempKataDasar, daftar_kata)) {
                        //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                        words.remove(i);
                        words.add(i, tempKataDasar);
                        continue;
                    }
                }

            /* -----------------------------------------------------------------
				KD
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar;
                if (in_array(tempKataDasar, daftar_kata)) {
                    ////Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }

			/* -----------------------------------------------------------------
				AW II.KD.AKH III.AKH II.AKH I
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[1] + kataDasar + akhiran[2] + akhiran[1] + akhiran[0];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }



            /* -----------------------------------------------------------------
				KD + AK III
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar + akhiran[2];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }

            /* -----------------------------------------------------------------
				KD + AK II
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar + akhiran[1];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }

            /* -----------------------------------------------------------------
				KD + AK I
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar + akhiran[0];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }

			/* -----------------------------------------------------------------
				KD + AK III + AK II + AK I
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar + akhiran[2] + akhiran[1] + akhiran[0];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				KD + AK III + AK II
			--------------------------------------------------------------------*/
                tempKataDasar = kataDasar + akhiran[2] + akhiran[1];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }



			/* -----------------------------------------------------------------
				AW I.KD
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[0] + kataDasar;
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				AW I.AW II.KD
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[0] + awalan[1] + kataDasar;
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				AW I.AW II.KD.AKH III
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[0] + awalan[1] + kataDasar + akhiran[2];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				AW I.AW II.KD.AKH III.AKH II
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[0] + awalan[1] + kataDasar + akhiran[2] + akhiran[1];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }

			/* -----------------------------------------------------------------
				AW II.KD
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[1] + kataDasar;
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				AW II.KD.AKH III
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[1] + kataDasar + akhiran[2];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
			/* -----------------------------------------------------------------
				AW II.KD.AKH III.AKH II
			--------------------------------------------------------------------*/
                tempKataDasar = awalan[1] + kataDasar + akhiran[2] + akhiran[1];
                if (in_array(tempKataDasar, daftar_kata)) {
                    //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                    words.remove(i);
                    words.add(i, tempKataDasar);
                    continue;
                }
                //=======
                //Log.d("Preprocessor", "Stemming: " + words.get(i) + " to " + tempKataDasar);
                //words.remove(i);
                //words.add(i, tempKataDasar);
                //continue;
            }

        }
        return words;

    }

    public static boolean in_array(String needle, String[] haystack) {
        for (String aHaystack : haystack) {
            if (aHaystack.equalsIgnoreCase(needle))
                return true;
        }
        return false;
    }

}

