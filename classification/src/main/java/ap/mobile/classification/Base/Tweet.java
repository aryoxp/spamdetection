package ap.mobile.classification.Base;

import java.util.ArrayList;

/**
 * Created by aryo on 5/8/16.
 */
public class Tweet {

    public enum TweetCategory{
        UNKNOWN, SPAM, HAM
    }

    public String id;
    public String name;
    public String text;
    public String screenName;
    public String profileImageUrl;
    public String createdAt;
    public TweetCategory category = TweetCategory.UNKNOWN;
    public TweetCategory label = TweetCategory.UNKNOWN;

    public String getScreenName() {
        return "@"+screenName;
    }
    public ArrayList<String> words;

}