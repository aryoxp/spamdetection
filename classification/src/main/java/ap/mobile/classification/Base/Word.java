package ap.mobile.classification.Base;

/**
 * Created by aryo on 6/8/16.
 */
public class Word {

    public String text;
    public int fSpam;
    public int fHam;

    public double priorHam;
    public double priorSpam;

    public Word(String text) {
        this.text = text;
        this.fHam = 0;
        this.fSpam = 0;
    }

    public Word(String text, int fHam, int fSpam) {
        this.text = text;
        this.fHam = fHam;
        this.fSpam = fSpam;
    }

}
