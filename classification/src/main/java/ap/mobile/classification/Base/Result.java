package ap.mobile.classification.Base;

import java.util.ArrayList;

/**
 * Created by aryo on 5/8/16.
 */
public class Result {

    private ArrayList<Tweet> spams;
    private ArrayList<Tweet> hams;

    public Result(){

        this.spams = new ArrayList<>();
        this.hams = new ArrayList<>();

    }

    public void addSpam(Tweet spam) {
        this.spams.add(spam);
    }

    public void addHam(Tweet ham) {
        this.hams.add(ham);
    }

    public ArrayList<Tweet> getSpams() {
        return this.spams;
    }

    public ArrayList<Tweet> getHams() {
        return this.hams;
    }

}
