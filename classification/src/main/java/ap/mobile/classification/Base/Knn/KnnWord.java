package ap.mobile.classification.Base.Knn;

import java.util.ArrayList;

import ap.mobile.classification.Base.Tweet;

/**
 * Created by aryo on 4/1/17.
 */
public class KnnWord {

    public String word;
    public int tf;
    public double tfidfSpam;
    public double tfidfHam;

    public KnnWord(String word) {
        this.word = word;
    }

    public void calculateTFIDF(ArrayList<KnnTweet> tweetDocuments) {

        int numInSpamDocs = 0;
        int numInHamDocs = 0;
        int numSpamDocs = 0;
        int numHamDocs = 0;
        for (KnnTweet t: tweetDocuments) {
            if(t.category == Tweet.TweetCategory.SPAM) {
                numSpamDocs++;
                for (KnnWord word : t.words) {
                    if (word.word.equals(this.word)) {
                        numInSpamDocs++;
                        break;
                    }
                }
            } else if(t.category == Tweet.TweetCategory.HAM) {
                numHamDocs++;
                for (KnnWord word : t.words) {
                    if (word.word.equals(this.word)) {
                        numInHamDocs++;
                        break;
                    }
                }
            }
        }
        if(numInSpamDocs == 0) this.tfidfSpam = 0;
        else this.tfidfSpam = this.tf * Math.log(numSpamDocs/numInSpamDocs);

        if(numInHamDocs == 0) this.tfidfHam = 0;
        else this.tfidfHam = this.tf * Math.log(numHamDocs/numInHamDocs);

    }

    public static ArrayList<KnnWord> fromWords(ArrayList<String> wordStrings) {
        ArrayList<KnnWord> words = new ArrayList<>();
        for (String word : wordStrings)
            words.add(new KnnWord(word));
        return words;
    }

}
