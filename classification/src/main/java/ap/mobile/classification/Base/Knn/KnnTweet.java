package ap.mobile.classification.Base.Knn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ap.mobile.classification.Base.Tweet;

/**
 * Created by aryo on 4/1/17.
 */

public class KnnTweet extends Tweet {

    public ArrayList<KnnWord> words = new ArrayList<>();
    public double tfidfHam = 0;
    public double tfidfSpam = 0;

    public ArrayList<DocDistance> docDistances;

    public void calculateTF() {
        for(KnnWord word : this.words) {
            word.tf = 1;
            for(KnnWord tword: this.words) {
                if(word.word.equals(tword.word))
                    word.tf++;
            }
        }
    }

    public static KnnTweet from(Tweet t) {
        KnnTweet knnTweet = new KnnTweet();
        knnTweet.id = t.id;
        knnTweet.name = t.name;
        knnTweet.text = t.text;
        knnTweet.screenName = t.screenName;
        knnTweet.profileImageUrl = t.profileImageUrl;
        knnTweet.createdAt = t.createdAt;
        knnTweet.category = TweetCategory.UNKNOWN;
        knnTweet.label = t.label;
        if(t.words != null) {
            for (String word : t.words) {
                KnnWord w = new KnnWord(word);
                knnTweet.words.add(w);
            }
        } else knnTweet.words = null;
        return knnTweet;
    }

    public static ArrayList<KnnTweet> from(ArrayList<Tweet> tweets) {
        ArrayList<KnnTweet> knnTweets = new ArrayList<>();
        for (Tweet t: tweets) {
            knnTweets.add(KnnTweet.from(t));
        }
        return knnTweets;
    }

    public class DocDistance {
        public Tweet doc;
        public double distance;
    }

    public void categorize(ArrayList<KnnTweet> documents) {

        this.docDistances = new ArrayList<>();

        for(KnnTweet t: documents) {

            DocDistance docDistance = new DocDistance();
            docDistance.doc = t;
            docDistance.distance = Math.sqrt(
                    Math.pow(t.tfidfHam - this.tfidfHam, 2)
                    + Math.pow(t.tfidfSpam - this.tfidfSpam, 2)
            );

            this.docDistances.add(docDistance);

        }

        Collections.sort(this.docDistances, new Comparator<DocDistance>() {
            @Override
            public int compare(DocDistance docDistance, DocDistance t1) {
                if (docDistance.distance == t1.distance) return 0;
                return (docDistance.distance > t1.distance)? 1 : -1;
            }
        });

        int spam = 0;
        int ham = 0;
        for(int i=0;i<10;i++) {
            if(docDistances.get(i).doc.category == TweetCategory.SPAM) spam++;
            else ham++;
        }

        if(spam > ham)
            this.category = TweetCategory.SPAM;
        else this.category = TweetCategory.HAM;

    }

}
