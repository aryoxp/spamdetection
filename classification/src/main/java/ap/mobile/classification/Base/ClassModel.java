package ap.mobile.classification.Base;

import java.util.ArrayList;

/**
 * Created by aryo on 5/8/16.
 */
public class ClassModel {

    public enum Category {
        SPAM, HAM
    }

    public ArrayList<Word> words = new ArrayList<>();

    private int wordCountHam;
    private int wordCountSpam;

    private int numHam;
    private int numSpam;

    public void addHam() {
        this.numHam++;
    }

    public void addSpam() {
        this.numSpam++;
    }

    public void addWord(String word, Category category) {
        for (int i = 0; i < words.size(); i++) {
            Word w = words.get(i);
            if (w.text.equals(word)) {
                if (category == Category.HAM)
                    w.fHam++;
                else w.fSpam++;
                return;
            }
        }

        Word newWord = (category == Category.HAM) ? new Word(word, 1, 0) : new Word(word, 0, 1);
        this.words.add(newWord);

    }

    public void updatePrior() {

        for (Word w:this.words) {
            wordCountHam += w.fHam;
            wordCountSpam += w.fSpam;
        }

        for (Word w:this.words) {
            w.priorHam = (double)(w.fHam + 1) / (double)(this.wordCountHam + this.words.size());
            w.priorSpam = (double)(w.fSpam + 1) / (double)(this.wordCountSpam + this.words.size());
            //newWords.add(w);
        }

    }

    private double pSpam() {
        return this.numSpam / (double) (this.numHam + this.numSpam);
    }

    private double pHam() {
        return this.numHam / (double) (this.numHam + this.numSpam);
    }

    public double getPrior(String word, Category category) {
        for(Word w: this.words) {
            switch (category) {
                case HAM:
                    if (w.text.equalsIgnoreCase(word))
                        return w.priorHam;
                case SPAM:
                    if (w.text.equalsIgnoreCase(word))
                        return w.priorSpam;
            }
        }
        for(Word w: this.words) {
            switch (category) {
                case HAM:
                    if(w.fHam == 0)
                        return w.priorHam;
                case SPAM:
                    if(w.fSpam == 0)
                        return w.priorSpam;
            }
        }
        return 0;
    }

    public double getPosterior(ArrayList<String> words, Category category) {
        double posterior;
        switch (category) {
            case SPAM:
                posterior = pSpam();
                for(String word: words) {
                    posterior *= getPrior(word, Category.SPAM);
                }
                return posterior;
            default:
                posterior = pHam();
                for(String word: words) {
                    posterior *= getPrior(word, Category.HAM);
                }
                return posterior;
        }
    }

}
